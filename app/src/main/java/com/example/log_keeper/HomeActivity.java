package com.example.log_keeper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class HomeActivity extends AppCompatActivity {


    //Declaring Variables
    Button btnteacher,btnstudent;
    ImageView imgpic;
    TextView txtname, txtemail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Initializing Variables
        btnteacher = findViewById(R.id.btnTeacher);
        btnstudent = findViewById(R.id.btnStudent);
        txtname = findViewById(R.id.txtName);
        txtemail = findViewById(R.id.txtEmail);
        imgpic =findViewById(R.id.profPic);

        //Getting Data
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
       // String photo = intent.getStringExtra("photo");

        //Posting data
        txtname.setText("Name -> "+name);
        txtemail.setText("Email ID -> "+email);
       // Picasso.with(HomeActivity.this).load(photo).into(imgpic);

        //Button function
        btnteacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }
}
