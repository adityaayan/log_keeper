package com.example.log_keeper;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {

    //Declaring Variables
    Button btn_signin , btn_signout;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 123;
    ProgressBar progressBar;
    String name,email,photo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Assigning Variables
        btn_signin = findViewById(R.id.login);
        btn_signout =findViewById(R.id.logout);
        progressBar = findViewById(R.id.prgsbar);


        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 signIn();
            }
        });


        btn_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        // Configure sign-in to request the user's ID, email address, and basic
       // profile. ID and basic profile are included in DEFAULT_SIGN_IN.


        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    //Method for Signin
    public void signIn()
    {
        progressBar.setVisibility(View.VISIBLE);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    //Method for Signin
    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        btn_signin.setVisibility(View.VISIBLE);
                        btn_signout.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "logged out successfully", Toast.LENGTH_SHORT).show();

                    }
                });
    }


    //Method for goint to the next activity
    private void nextActivity()
    {

        try {
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("email", email);
            intent.putExtra("photo", photo);
            Toast.makeText(getApplicationContext(),"came",Toast.LENGTH_SHORT).show();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Not executed",Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    protected void onStart() {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
        super.onStart();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {

            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            progressBar.setVisibility(View.INVISIBLE);

            // Signed in successfully, show authenticated UI.

            updateUI(account);

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            progressBar.setVisibility(View.INVISIBLE);
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }



    //to update the activity after sign in and sign out
    private void updateUI(GoogleSignInAccount user)
    {
        if (user != null) {
            name = user.getDisplayName();
            email = user.getEmail();
            photo = String.valueOf(user.getPhotoUrl());

            btn_signout.setVisibility(View.VISIBLE);
            btn_signin.setVisibility(View.INVISIBLE);

            nextActivity();
        }
        else{
            Toast.makeText(getApplicationContext(), "Log In with Your Google Account", Toast.LENGTH_SHORT).show();
        }

    }


}
